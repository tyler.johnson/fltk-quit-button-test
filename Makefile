src := quit-button.cpp

first:
	mkdir -p release
	g++ --std=c++17 $(src) -o release/quit-button $(shell fltk-config --ldflags)
	release/quit-button
