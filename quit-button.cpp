#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Widget.H>

int main(int argc, char ** argv) {
  constexpr auto winWidth = 320;
  constexpr auto winHeight = 180;

  const auto screenWidth = Fl::w();
  const auto screenHeight = Fl::h();

  const auto winPosX = (screenWidth - winWidth) / 2;
  const auto winPosY = (screenHeight - winHeight) / 2;

  Fl_Window *window = new Fl_Window(winPosX, winPosY, winWidth, winHeight);
  Fl_Button *button = new Fl_Button(10,40,300,100,"&Quit");

  button->labelfont(FL_BOLD+FL_ITALIC);
  button->labelsize(36);
  button->labeltype(FL_SHADOW_LABEL);
  button->callback([](Fl_Widget *w) {
    Fl::first_window()->hide();
  });

  window->end();
  window->show(argc, argv);
  return Fl::run();
}
